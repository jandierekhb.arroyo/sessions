


const express = require("express");

const app = express();
const port = 4000;


// Middlewares 

app.use(express.json());

// Allows your app to read data from forms
app.use(express.urlencoded({extended: true}));

// accepts all data type 


if(require.main === module){

    app.listen(port, () => console.log('Server is running at port ${port}'));

}

// [SECTION ROUTES]


// GET METHOD
app.get("/", (req, res) => {
    res.send("Hello World!")
});
 

app.get("/hello", (req, res) => {
    res.send("Hello from /hello end point.");
})



//   route expects to receive a POST method at the URI "/hello"
app.post("/hello", (req, res) => {
    res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`)
})

//POST route to register user

let users = [];


app.post("/signup", (req, res) => {
    console.log(req.body);
    const usernameExists = users.some(user => user.username === req.body.username);

    if (!req.body || !req.body.username || !req.body.password) {
        res.send("Please input BOTH username and password");
        return;
    } else if (usernameExists) {
        res.send(`Username ${req.body.username} is already registered.`);
    } else {
        // Add the new user to the array
        users.push(req.body);
        res.send(`User ${req.body.username} successfully registered!`);
    }
});


//  Change password - put method
app.put("/change-password", (req, res) => {
    let message;
    let userFound = false; // A flag to check if a user was found
    
    for (let i = 0; i < users.length; i++) {
        if (req.body.username === users[i].username) {
            users[i].password = req.body.password;
            message = `User ${req.body.username}'s password has been updated.`;
            userFound = true; // Set the flag to true when a user is found
            break;
        }
    }

    if (!userFound && users.length === 0) {
        message = "No user is currently registered.";
    } else if (!userFound) {
        message = "No user with the provided username found.";
    }

    res.send(message);
});


