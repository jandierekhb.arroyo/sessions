// [SECTION] Comparison Query Operator

// $gt / $gte operator - greater than

/*
SYNTAX:

db.collectionName.find({field : {$gt : value}})
db.collectionName.find({field : {$gte : value}})
*/


db.users.find({age : {$gt : 50 }});
db.users.find({age : {$gte : 50}});


// $lt / $lte operator - less than

/*
SYNTAX:

db.collectionName.find({field : {$lt : value}})
db.collectionName.find({field : {$lte : value}})
*/


db.users.find({age : {$lt : 50 }});
db.users.find({age : {$lte : 50}});



// $ne operator - not equal

/*
SYNTAX:

db.collectionName.find({field : {$ne : value}})
*/


db.users.find({age : {$ne : 82 }});



// $in - inside

/*
SYNTAX:

db.collectionName.find({field : {$in : value}})

*/


db.users.find({lastName : {$in : ["Hawking", "Doe"]}});
db.users.find({courses : {$in : ["HTML", "React"]}});

// [SECTION] Logical Query Operator



/*
SYNTAX:

db.collectionName.find({$or: [{fieldnA: value}, {fieldB: value}]})

*/


db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

// or with gt


db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

// $and operator


/*
SYNTAX:

db.collectionName.find({$and: [{fieldnA: value}, {fieldB: value}]})

*/


db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});


// [SECTION] Field Projecton 

// Inclusion

/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 to denote that the field is being included.
    - Syntax
        db.users.find({criteria},{field: 1})
*/

db.users.find
(

        {
            firstName: "Jane"
        },

        {
        firstName: 1,
        lastName: 1,
        contact: 1    
        }
      
    
);

// Exclusion

db.users.find
(

        {
            firstName: "Jane"
        },

        {
            firstName: 0,
            contact: 0,
            department: 0,
        }
         
);

//  Suppresing the ID field

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1,
        _id: 0
    }
);


// [SECTION] Evaluation Query Operator

// $regex operator

/*
        db.users.find({field: $regex : "pattern", $options : "$optionsValue"})
*/

// Case sensitve Query

db.users.find({firstName : {$regex: "N"}});

// Mongo DB v

db.users.find({firstName : {$regex: "j", $options: "i"}});

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        "contact.email": 1
        
    }
);
