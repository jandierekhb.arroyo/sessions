let http = require('http');

let port = 4000;

let app = http.createServer(function (request, response){

	if(request.url === "/items" && request.method === "GET"){

		// 
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// Ends the reponse process
		response.end('Data retrived from the database');
	};

	// The method "POST"
	if(request.url == "/items" && request.method == "POST"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data to be sent to the database');
	};
})

// Inform us if the server is running, by printing our message:
// First Arguement, the port number to assign the server
// Second Arguement, the callback/function to run when the server is running
app.listen(port, () => console.log('Server is running at localhost:4000'));


let http = require("http");

const app = http.createServer(function (request, response) {

    // (1)Front End --> GET Method
    // (2)Back End --> READ Method --> .find()
    // (3)DB

    // (1)Front End --> POST Method
    // (2)Back End --> CREATE Method --> .Insert()
    // (3)DB

     // (1)Front End --> PUT Method
    // (2)Back End --> UPDATE Method --> .Update()
    // (3)DB

    // (1)Front End --> DELETE Method
    // (2)Back End --> DELETE Method --> .Update()
    // (3)DB


    if(request.url == "/" && request.method == "GET"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Welcome to booking system');

    }

    if(request.url == "/profile" && request.method == "GET"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Welcome to your profile');

    }

    // Retrieving a course
    if(request.url == "/courses" && request.method == "GET"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end(`Here's our courses available`);

    }

    // Adding a new course
    if(request.url = "/addCourse" && request.method == "POST"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Add course to our resources');

    }

    // Updating a course
    if(request.url = "/updateCourse" && request.method == "PUT"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Update a course to our resources');

    }

    // Deleting a course
    if(request.url = "/archiveCourses" && request.method == "DELETE"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Archive courses to our resources');

    }

})

//Do not modify
//Make sure to save the server in variable called app

if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;