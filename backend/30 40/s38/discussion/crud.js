const http = require('http');

const directory = [
    {
        "name": "Brandon",
        "email": "brandon@mail.com",
    },
    {
        "name": "Jobert",
        "email": "jobert@mail.com",
    }
];

http.createServer((req, res) => {
    if (req.url === "/users" && req.method === "GET") {
        res.writeHead(200, {'Content-Type': 'application/json'});

        try {
            res.write(JSON.stringify(directory));
        } catch (error) {
            console.error("Error sending GET response:", error);
            res.writeHead(500, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({ error: "Internal server error" }));
        }

        res.end();
    }

    if (req.url === "/users" && req.method === "POST") {
        let requestBody = "";

        req.on('data', (data) => {
            requestBody += data;
            console.log("Received data:", data.toString());
            console.log("Current requestBody:", requestBody);
        });

        req.on('end', () => {
            console.log("Final requestBody:", requestBody);

            try {
                requestBody = JSON.parse(requestBody);
                const newUser = {
                    "name": requestBody.name,
                    "email": requestBody.email
                };

                console.log("New User:", newUser);
                directory.push(newUser);

                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify(newUser));
            } catch (error) {
                console.error("JSON parsing error:", error);
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({ error: "Invalid JSON data" }));
            }

            res.end();
        });
    }
}).listen(4000);

console.log('Server is running at localhost:4000!');
