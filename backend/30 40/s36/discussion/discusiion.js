db.fruits.insertMany([
    {
      name : "Apple",
      color : "Red",
      stock : 20,
      price: 40,
      supplier_id : 1,
      onSale : true,
      origin: [ "Philippines", "US" ]
    },

    {
      name : "Banana",
      color : "Yellow",
      stock : 15,
      price: 20,
      supplier_id : 2,
      onSale : true,
      origin: [ "Philippines", "Ecuador" ]
    },

    {
      name : "Kiwi",
      color : "Green",
      stock : 25,
      price: 50,
      supplier_id : 1,
      onSale : true,
      origin: [ "US", "China" ]
    },

    {
      name : "Mango",
      color : "Yellow",
      stock : 10,
      price: 120,
      supplier_id : 2,
      onSale : false,
      origin: [ "Philippines", "India" ]
    }
  ]);


// Aggregate Methods

/*

SYNTAX: 

- {$match: {field: value}}
- $group - group elements together

db.collectionName.aggregate
([
    {
        $match : {fieldA: valueA},
    }
    {   
        $group : {_id: "$field"}, {result: {operation}}
    }
])


*/

db.fruits.aggregate
([
     {
         $match : {onSale: true}
     },
  
     { 
        $group : {_id: "$supplier_id", total: {$sum: "$stock"}}
     }
]);


// Field Projection

// $project - can be used when aggreagating data to include/exclude fields from the returned results.

// $project values: 1/0

db.fruits.aggregate
([
     {
         $match : {onSale: true}
     },
  
     { 
        $group : {_id: "$supplier_id", total: {$sum: "$stock"}}
     },

     {
        $project: {_id: 0}
     }
]);

// Sorting Aggregated result
// $sort values 1/-1

db.fruits.aggregate
([
     {
         $match : {onSale: true}
     },
  
     { 
        $group : {_id: "$supplier_id", total: {$sum: "$stock"}}
     },

     {
        $project: {_id: 0}
     },

     {
        $sort: {total: -1}
     }
]);


// Aggregating results based on array fields

//  {$unwind: field} 

db.fruits.aggregate([
    {
        $unwind: "$origin"
    }
]);

// Display fruit documents by their origin and the kinds of fruits that are supplied 

db.fruits.aggregate
([
    {
        $unwind: "$origin"
    },
  
    { 
        $group : {_id: "$origin", kinds: {$sum: 1}}
    },

]);