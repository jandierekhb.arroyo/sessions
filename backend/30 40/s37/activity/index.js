//Add solution here


const http = require('http');

const port = 3000;


const app = http.createServer((req, res) => {
  if (req.url === '/login') {
    // Route for /login
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('You are in the login page.\n');
  } else {
    // Any other route
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Error: Route not found.\n');
  }
});

app.listen(port, () => {
  console.log(`app is running on port ${port}`);
});




//Do not modify
module.exports = {app}