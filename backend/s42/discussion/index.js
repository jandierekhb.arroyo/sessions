const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require("./routes/taskRoute.js");


//  Server setup + middleware
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// default endpoint
app.use("/tasks", taskRoute);

// DB connection

// If connection is succesful, output in console
let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error!"));
db.once("open", () => console.log("We're connected to the cloud database."));









// Server Listerning

// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${ process.env.PORT || port }`);
	});
};