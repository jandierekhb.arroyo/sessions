const Task = require("../models/Task.js");


module.exports.getAllTask = () => {
    return Task.find({}).then(result => {
        return result;
    })
}


module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        // req.body.name === requestBody.name
        name: requestBody.name
    })

    return newTask.save().then((task, error) => {
        if(error) {
            console.log(error);
        }else {
            return task;
        }
    })

};



module.exports.deleteTask = (taskID) => {
    return Task.findByIdAndRemove(taskID).then((removedTask, error) =>
    {
        if(error){
            console.log(error);
        }else{
            return removedTask;
        }
    })
}




module.exports.updateTask = (taskID, requestBody) => {
    return Task.findById(taskID).then((result, error) =>
    {
        if(error){
            console.log(error);
            return res.send("Cannot find data with the provided ID");
        }
             result.name = requestBody.name;
            
            return result.save().then((updatedTask, error) =>
                {
                    if(error){
                        console.log(error);
                        return res.send("There is error while saving in this ");
                    } else {
                        return updatedTask;
                    }
                }
            );
    })
}