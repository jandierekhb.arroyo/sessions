const express = require('express');
const { default: mongoose } = require('mongoose');
// Allow access to HTTP Methods and middlewares
const router = express.Router();
const taskController = require("../controllers/taskController.js");
// Get all tasks
router.get("/", (req, res) =>
    {
        taskController.getAllTask().then(resultFromController => res.send(resultFromController))
    }
);


//  Create Task
router.post("/", (req, res) =>
    {
        taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
    }
)

//  Delete a Task using a wildcard on params
router.delete("/:id", (req, res) =>
    {
        taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
    }
)

router.put("/:id", (req, res) =>
    {
        taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
    }
)


module.exports = router;