// Non-Mutator Methods

let countries = [
    "US",
    "PH",
    "CAN",
    "SG",
    "TH",
    "PH",
    "FR",
    "DE"
];

// indexOf();

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf " + invalidCountry);

// lastIndexOf()

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of indexOf " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of indexOf " + lastIndexStart);

//  slice()

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);



// toString()
let stingArray = countries.toString();
console.log("Result of toString " + stingArray);

// concat()

let taskArrayA = ["Drink html", "Eat javascript"];
let taskArrayB = ["inhale css", "Breath Sass"];
// let taskArrayC= ["Drink html", "Eat javascript"];

let tasks = taskArrayA.concat(taskArrayB);
tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

let combineTasks = taskArrayA.concat("Smell Express", "throw react");
console.log(combineTasks);

let users = ["John", "Jane", "Joe", "Robert"];

console.log(users.join());
console.log(users.join(""));
console.log(users.join("-"));
console.log(users.join("x"));

// forEach()

tasks.forEach(task => {
    console.log(task + " FiFI");

});

tasks.forEach(function (task) {
    console.log(task);
});

let filteredTasks = [];

// tasks.forEach(function (task) {
//     if (task.length > 10) {
//         filteredTasks.push(task)
//     }
// });

tasks.forEach((task) => {
    if (task.length > 10) {
        filteredTasks.push(task)
    }
});

console.log("Result of Filtered Task ");
console.log(filteredTasks);

// every()

let numbers = [1, 2, 3, 4, 5]
// let allValid = numbers.every(function (number) {
//     return (number < 3);
// });

let allValid = numbers.every(number => {
    return (number < 3);
});


console.log(allValid);


// let someValid = numbers.some(function(number){
//     return (number < 3)
// });
let someValid = numbers.some(number =>{
    return (number < 3)
});

console.log(someValid);

//  filter()

// let filterValid = numbers.filter(function(number){
//     return (number <=3)
// })

let filterValid = numbers.filter(number =>{
    return (number <=3)
})

console.log(filterValid);

// includes()

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);


// Additional example

// let filteredProducts = products.filter(function(product){
//     return product.toLocaleLowerCase().includes("a");
// })


// console.log(filteredProducts);
// testing arrow function

let filteredProducts = products.filter(product =>{
    return product.toLocaleLowerCase().includes("a");
})


console.log(filteredProducts);

const originalString = "Hello, World!";
const slicedString = originalString.slice(7, 12); 
console.log(slicedString);