

let registeredPokemon = [];

// add pokemon to array

function addPokemon(pokemonName){
  registeredPokemon.push(pokemonName);
}


addPokemon("Pikachu");
addPokemon("Charizard");
addPokemon("Bulbasaur");


console.log(registeredPokemon);

// delete

function deletePokemon(){
    if(registeredPokemon.length > 0)
    {
        registeredPokemon.pop();
    } else {
        console.log("No pokemon registered.");
    }
    // registeredPokemon.length > 0 ? registeredPokemon.pop() : console.log("No pokemon registered.");
};


deletePokemon();
deletePokemon();
deletePokemon();
deletePokemon();


console.log(registeredPokemon);


addPokemon("Dialga");

// displayNumber of pokemons
function displayNumberOfPokemons() {
    if(registeredPokemon.length === 0) {
        console.log("No registered pokemon.");
    } else if(registeredPokemon.length !== 0)
    {
        console.log(registeredPokemon.length); 
    }
    // registeredPokemon.length === 0 ? console.log("No registered pokemon.") : console.log(registeredPokemon.length);

}

displayNumberOfPokemons();

addPokemon("Eve");
addPokemon("Zapdoz");
addPokemon("Ponyta");
addPokemon("Garbodor ");


// Sort


function sortPokemon(){
    if(registeredPokemon.length === 0) {
        console.log("No pokemon registered.");
    } else if(registeredPokemon.length > 0)
    {
       console.log(registeredPokemon.sort()); 
    }
    // registeredPokemon.length === 0 ? console.log("No pokemon registered.") : console.log(registeredPokemon.slice().sort());

}

sortPokemon();


// Registered Trainers

let registeredTrainers = [];

function registerTrainer(name, level, pokemon){
    let trainer = {
        trainerName: name,
        trainerLevel: level,
        pokemons: pokemon
    }
    registeredTrainers.push(trainer)
}

registerTrainer("Jan Dierekh", 30, registeredPokemon)

registerTrainer("Bryan Hernandez", 25, ["Ponyta","Charmander","Zapdos"])
console.log(registeredTrainers);




//Do not modify
//For exporting to test.js
try{
    module.exports = {

        registeredPokemon: typeof registeredPokemon !== 'undefined' ? registeredPokemon : null,
        registeredTrainers: typeof registeredTrainers !== 'undefined' ? registeredTrainers : null,
        addPokemon: typeof addPokemon !== 'undefined' ? addPokemon : null,
        deletePokemon: typeof deletePokemon !== 'undefined' ? deletePokemon : null,
        displayNumberOfPokemons: typeof displayNumberOfPokemons !== 'undefined' ? displayNumberOfPokemons : null,
        sortPokemon: typeof sortPokemon !== 'undefined' ? sortPokemon : null,
        registerTrainer: typeof registerTrainer !== 'undefined' ? registerTrainer : null

    }
} catch(err){

}