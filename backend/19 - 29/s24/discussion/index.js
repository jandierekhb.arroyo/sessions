// console.log("Hello");  While Loop let count = 5; while(count !== 0){
// console.log("While: " + count);     count--; }; let number =
// Number(prompt("Give  me a number.")); do{     console.log("Do while: " +
// number);     number++; }while( number <= 1) { }; for(let count = 0; count <=
// 20; count++) {     console.log(count); }

let myString = "alex";

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for (let x = 0; x < myString.length; x++) {
    console.log(myString[x]);
}

let myName = "AlEx";

for (let i = 0; i < myName.length; i++) {
    // console.log(myName[i].toLocaleLowerCase());

    if (myName[i].toLocaleLowerCase() == 'a' || myName[i].toLocaleLowerCase() == 'e' || myName[i].toLocaleLowerCase() == 'i' || myName[i].toLocaleLowerCase() == 'o' || myName[i].toLocaleLowerCase() == 'u') {
        console.log(3);
    } else {
        console.log(myName[i]);
    }

}


for (let index = 0; index <= 20; index++) {
    
    if(index % 2 === 0)
    {
        continue;
    }
    console.log("Continue and Break: " + index);
    if (index > 10) {
        break;
    }
    
}

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
   console.log(name[i]);
    
   if(name[i].toLowerCase() === "a"){
    console.log("Continue  to the next iteration");
   }

    if(name[i] === "d"){
        break;
    }
}
