// Sum
function addNum(addend1, addend2) {
    return addend1 + addend2;
}

const addend1 = parseFloat(prompt("Enter the first addend:"));
const addend2 = parseFloat(prompt("Enter the  second addend:"));

const sum = addNum(addend1, addend2)

console.log("Displayed Sum of " + addend1 + " and " + addend2);
console.log(sum);
// Difference
function subNum(minuend, subtrahend) {
    return minuend - subtrahend;
}

const minuend = parseFloat(prompt("Enter the minuend:"));
const subtrahend = parseFloat(prompt("Enter the subtrahend:"));

const difference = subNum(minuend, subtrahend)

console.log("Displayed Difference of " + minuend + " and " + subtrahend);
console.log(difference);
// product
function multiplyNum(factor1, factor2) {
    return factor1 * factor2;
}

const factor1 = parseFloat(prompt("Enter the first factor:"));
const factor2 = parseFloat(prompt("Enter the second factor:"));

const product = multiplyNum(factor1, factor2)

console.log("Displayed Product of " + factor1 + " and " + factor2);
console.log(product);
// Quotient
function divideNum(dividend, divisor) {
    return dividend / divisor;
}

const dividend = parseFloat(prompt("Enter the dividend:"));
const divisor = parseFloat(prompt("Enter the divisor:"));

const quotient = divideNum(dividend, divisor)

console.log("Displayed Quotient of " + dividend + " and " + divisor);
console.log(quotient);

function getCircleArea(circleArea) {
    const area = Math.PI * (circleArea ** 2);
    return area.toFixed(2);
}

const circleArea = parseFloat(prompt("Please input the radius: "));
console.log(
    "The result of getting the area of a circle with " + circleArea +
    " radius: \n" + getCircleArea(circleArea)
);

function getAverage(num1, num2, num3, num4) {
    const totalsum = num1 + num2 + num3 + num4;
    return totalsum / 4;
}

const input = prompt("Enter four numbers separated by Comma :");
const [input1, input2, input3, input4] = input
    .split(",")
    .map(parseFloat);
const averageVar = getAverage(input1, input2, input3, input4);

console.log(
    "The average of " + input1 + "," + input2 + "," + input3 + "," + input4 +
    ": \n" + averageVar
);

function checkIfPassed(score, totalscore) {
    const percentage = ((score / totalscore) * 100)
    let isPassed;

    if (percentage >= 75) {
        return isPassed = true;
    } else {
        return isPassed = false;
    }
}

const AskScore = prompt(
    "Input the your score then slash (/) lastly input the Total Score of your test"
);
const [score, totalscore] = AskScore
    .split("/")
    .map(parseFloat);
const isPassingScore = checkIfPassed(score, totalscore);

console.log(
    "Is " + score + "/" + totalscore + " a passing score? \n" + isPassingScore
);

//Do not modify For exporting to test.js
// Note: Do not change any variable and function names. All variables and
// functions to be checked are listed in the exports.
try {
    module.exports = {

        addNum: typeof addNum !== 'undefined'
            ? addNum
            : null,
        subNum: typeof subNum !== 'undefined'
            ? subNum
            : null,
        multiplyNum: typeof multiplyNum !== 'undefined'
            ? multiplyNum
            : null,
        divideNum: typeof divideNum !== 'undefined'
            ? divideNum
            : null,
        getCircleArea: typeof getCircleArea !== 'undefined'
            ? getCircleArea
            : null,
        getAverage: typeof getAverage !== 'undefined'
            ? getAverage
            : null,
        checkIfPassed: typeof checkIfPassed !== 'undefined'
            ? checkIfPassed
            : null
    }
} catch (err) {}