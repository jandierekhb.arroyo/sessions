// [ SECTION ] Dependencies and Modules
const express = require('express');
const auth = require("../auth.js")
const userController = require('../controllers/user');

// Destructure from auth
const {verify, verifyAdmin} = auth;

// [ SECTION ] Routing Component
const router = express.Router();

// check email routes
router.post('/checkEmail', async (req, res) => {
    try {
        const resultFromController = await userController.checkEmailExist(req.body);
        res.send(resultFromController);
    } catch (error) {
        res
            .status(500)
            .send({error: 'An error occurred while processing your request.'});
    }
});

// Register a new user
router.post("/register", async (req, res) => {
    try {
        const resultFromController = await userController.registerUser(req.body);
        res.send(resultFromController);
    } catch (error) {
        res
            .status(500)
            .send({error: 'An error occurred while processing your request.'});
    }
});

// User authentication
router.post("/login", async (req, res) => {
    try {
        const resultFromController = await userController.loginUser(req.body);
        res.send(resultFromController);
    } catch (error) {
        res
            .status(500)
            .send({error: 'An error occurred while processing your request.'});
    }
});

// Retrieve user details
router.get("/details", verify, userController.getProfile);

// Enroll user to a course
router.post("/enroll", verify, userController.enroll)


// get enrollment of user's
router.get("/getEnrollments", verify, userController.getEnrollments)


// reset passwords
router.put("/reset-password", verify, userController.resetPassword)


// [ SECTION ] Export Route System
module.exports = router;