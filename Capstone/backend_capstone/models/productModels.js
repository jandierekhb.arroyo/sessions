const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required"],
        trim: true
    },
    SKU: {
        type: String,
        trim: true
    },
    category: {
        type: String,
        required: [true, "Please add a category"],
        trim: true
    },
    brand: {
        type: String,
        required: [true, "Please add a brand"],
        trim: true
    },
    color: {
        type: String,
        required: [true, "Please add a color"],
        default: "As seen",
        trim: true
    },
    quantity: {
        type: Number,
        required: [true, "Please add a quantity"],
    },
    sold: {
        type: Number,
        default: 0,
    },
    regularPrice: {
        type: Number,
    },
    originalPrice: {
        type: Number,
        required: [true, "Please add a price"],
    },
    description: {
        type: String,
        required: [true, "Please add a description"],
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("Product", productSchema); // Use "Product" as the model name
