const express = require('express');
const router = express.Router();
const { registerUser, loginUser, logoutUser, getUser, getLoginStatus, updateUser, userToAdmin, getUserAuthToken } = require('../controller/userController.js'); 
const asyncHandler = require('express-async-handler');
const {verify, adminOnly} = require("../middleware/authMiddleware")
// Register user
router.post('/register', asyncHandler(registerUser));

// Login user
router.post('/login', asyncHandler(loginUser));
// Logout user
router.post('/logout', asyncHandler(logoutUser));
// get User
router.get('/getUser', verify, asyncHandler(getUser) )

router.get('/getAuthToken', asyncHandler(getUserAuthToken));

router.get('/getLoginStatus', asyncHandler(getLoginStatus))

router.put('/updateUser', verify, asyncHandler(updateUser))

router.put('/userToAdmin', verify, adminOnly, asyncHandler(userToAdmin))




module.exports = router;
