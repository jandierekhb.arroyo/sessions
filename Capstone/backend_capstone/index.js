// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const userRoutes = require('./routes/userRoute.js')
const productRoute = require('./routes/productRoutes.js')
const orderRoute = require('./routes/orderRoutes.js');

const errorHandler = require('./middleware/errorMiddleware.js');
// This line loads environment variables from a .env file into process.env
require('dotenv').config();


// [ SECTION ] Environment Setup
const port = process.env.PORT || 4000;

// [ SECTION ] Server Setup
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser())
app.use(cors()); // Allows all resources to acces our backend application;

// [ SECTION ] Database Connection
const mongooseConnectionString = process.env.MONGODB_URI || "mongodb+srv://admin:admin123@cluster0.hocogbx.mongodb.net/capstone2?retryWrites=true&w=majority";
mongoose.connect(mongooseConnectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// error handling for MongoDB connection
const db = mongoose.connection;

db.on('error', (err) => { 
    console.error('MongoDB connection error:', err);
});
 
db.once('open', () => {
    console.log('Connected to MongoDB');
});

// Bash Boot Up nodemon

function onMongoDBConnectionOpen() {
    console.log('You are connected to MongoDB Atlas!');
}

mongoose
    .connection
    .once('open', onMongoDBConnectionOpen);



app.get('/', (req, res) => {
    res.send("Home Page..")
})

// app.use(bodyParser.json());

// [ SECTION ] Backend Routes
app.use('/users', userRoutes);
app.use('/product', productRoute); // I updated the route path
app.use('/order', orderRoute);



const api = process.env.API_URL;

// Error Middleware
app.use(errorHandler);


// [ SECTION ] Server Gateway Response

if (require.main === module) {
    const PORT = process.env.PORT || port;
    app.listen(PORT, () => {
        console.log(api);
        console.log(`API is now online on port ${PORT}`);
    });
}

module.exports = app;
