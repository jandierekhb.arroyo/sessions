const asyncHandler = require("express-async-handler");
const Product = require("../models/productModels.js");



const createProduct = asyncHandler(async (req, res, next) => {
    const {
        name,
        SKU, // Use capital SKU to match your model
        description,
        category,
        brand,
        quantity,
        originalPrice,
        regularPrice,
        color
    } = req.body;

    if (!name || !category || !brand || !quantity || !originalPrice || !description) {
        res.status(400);
        throw new Error("Please fill in all required fields");
    }

    // Check if a product with the same SKU already exists
    const productExist = await Product.findOne({ SKU: SKU });

    if (productExist) {
        const error = new Error("Product with the same SKU already exists.");
        res.status(400);
        return next(error); // Pass the error to your error-handling middleware and return
    }

    // Create the product if it doesn't exist
    const product = await Product.create({
        name,
        SKU, 
        category,
        brand,
        description,
        quantity,
        originalPrice,
        regularPrice,
        color
    });

    res.status(201).json(product);
});


//  get allproducts
const getAllproducts = asyncHandler(async (req, res) => {
    const product = await Product
        .find()
        .sort("-createdOn")
    res
        .status(201)
        .json(product);
});

const getProduct = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)

    if (!product) {
        res.status(404);
        throw new Error("Product not found. ");
    } else {
        res
            .status(201)
            .json(product);
    }
});

// Update Product
const updateProduct = asyncHandler(async (req, res) => {
    try {
        const {
            name,
            description,
            category,
            brand,
            quantity,
            originalPrice,
            regularPrice,
            color
        } = req.body;

        const product = await Product.findById(req.params.id);

        if (!product) {
            res.status(404);
            throw new Error("Product not found.");
        }

        // Update product
        const updatedProduct = await Product.findByIdAndUpdate(req.params.id, {
            name,
            description,
            category,
            brand,
            quantity,
            originalPrice,
            regularPrice,
            color
        }, {
            new: true,
            runValidators: true
        });

        if (!updatedProduct) {
            res.status(404);
            throw new Error("Product update failed or not found.");
        }

        res
            .status(201)
            .json(updatedProduct);
    } catch (error) {
        // Handle any errors that occur during the update process
        res
            .status(500)
            .json({error: error.message});
    }
});

// Archive a product

const archiveProduct = asyncHandler(async (req, res) => {
    try {

        let updateProduct = {
            isActive: false
        };

        const product = await Product.findByIdAndUpdate(
            req.params.productId,
            updateProduct
        );
        console.log(
            "Received request to archive product with ID:",
            req.params.productId
        );
        if (!product) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    } catch (error) {
        console.error("Error Archiving product:", error);
        return res
            .status(500)
            .send(false);
    }
});

// Activate a product
const activateProduct = asyncHandler(async (req, res) => {
    try {
        let updateProduct = {
            isActive: true
        };

        const product = await Product.findByIdAndUpdate(
            req.params.productId,
            updateProduct
        );
        console.log(
            "Received request to activate product with ID:",
            req.params.productId
        );
        if (!product) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    } catch (error) {
        console.error("Error Activating product:", error);
        return res.status(500).send(false);
    }
});


//  get allActiveproducts
const allActiveProducts = asyncHandler(async (req, res) => {
    try {
      const activeProducts = await Product.find({ isActive: true }).sort("-createdOn");
      
      if (activeProducts.length === 0) {
        // Handle the case where there are no active products
        return res.status(404).json({ message: "No active products found" });
      }
  
      res.status(200).json(activeProducts);
    } catch (error) {
      console.error("Error getting active products:", error);
      res.status(500).json({ error: "Internal Server Error" });
    }  
  });
  


//  get allArchiveproducts
const allArchiveproducts = asyncHandler(async (req, res) => {
    try {
      const activeProduct = await Product
          .find({isActive: false})
          .sort("-createdOn");
      res.status(201).json(activeProduct);
    } catch (error) {
          console.error("Error getting active products:", error);
          res.status(500).json({ error: "Internal Server Error" });
    }  
  });

const searchProductByName = asyncHandler(async (req, res) => {
    try {
      const  productName  = req.body.name;

    //   console.log(req.body);
  
      // Use a regular expression to perform a case-insensitive search
      const products = await Product.find({
        name: { $regex: productName, $options: 'i' }
      });
      

      res.json(products);
      
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
});

module.exports = {
    createProduct,
    getAllproducts,
    getProduct,
    archiveProduct,
    activateProduct,
    updateProduct,
    allActiveProducts,
    allArchiveproducts,
    searchProductByName
}