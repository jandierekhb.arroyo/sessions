const asyncHandler = require("express-async-handler");
const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken'); 
const { generateAuthToken } = require('../authenticationToken');


// Register user --controller--
const registerUser = asyncHandler(async (req, res) => {
    const { name, email, password, phoneNom } = req.body;

    // validation
    if (!name || !email || !password || !phoneNom) {
        res.status(400);
        throw new Error("Please fill in all required fields");
    }

    // checks if password is less than 6
    if (password.length < 6) { 
        res.status(400);
        throw new Error("Password must be up to 6 characters");
    }

    // Check if user exists
    const userExists = await User.findOne({ email: email });
    if (userExists) {
        res.status(400);
        throw new Error("Email has already been registered");
    }

    // Create a new user
    const user = await User.create({
        name,
        email,
        password,
        phoneNom // Include phoneNom when creating the user
    });

    // generate token
    const token = generateAuthToken(user._id);

    if (user) {
        const { _id, name, email, role } = user;
        res.cookie("token", token, {
            path: '/',
            httpOnly: true,
            expires: new Date(Date.now() + 1000 * 86400), 
            // secure: true,
            // sameSite: 'none', 
        });
        res.status(201).json({
            _id, name, email, token,
        });
    } else {
        res.status(400);
        throw new Error("Invalid user data");
    }
});

// Login User

const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body;
  
    switch (true) {
      case !email || !password:
        res.status(400);
        throw new Error("Email and password should be included.");
      case !email:
        res.status(400);
        throw new Error("Email is required.");
      case !password:
        res.status(400);
        throw new Error("Password is required.");
      default:
          // check if user exists
          const user = await User.findOne({ email });
          if(!user){
            res.status(400);
            throw new Error("User does not exist ");
          }
  
          // check if password is correct
          const passwordIsCorrect = await bcrypt.compare(password, user.password);
  
          // Generate token
          const token = generateAuthToken(user._id);
          if (user && passwordIsCorrect) {
              const newUser = await User.findOne({ email }).select("-password");
  
              // Include the token in the response JSON
              res.cookie("token", token, {
                  path: '/',
                  httpOnly: true,
                  expires: new Date(Date.now() + 1000 * 86400), 
                  // secure: true,
                  // sameSite: 'none', 
              });
            //   res.status(201).json({
            //     //   ...newUser.toObject(), 
            //       token, // Include the token
            //   });
              res.send({access: token})
          } else {
              res.status(400);
              throw new Error("Invalid email or Password");
          }
    }
  });
  
  


const logoutUser = asyncHandler(async (req, res) => {

    res.cookie("token", "", {
        path: '/',
        httpOnly: true,
        expires: new Date(0), })
        // secure: true,
        // sameSite: 'none', 
        res.status(200).json({message: "Successfully Log Out User"})
})

const getUser = asyncHandler(async (req, res) => {

   const user = await User.findById(req.user._id).select("-password")

   if (user) {
    res.send(user);
    } else {
    res.status(400);
    throw new Error("User not found");
    
}
})

// Get Login Status
const getLoginStatus = asyncHandler(async (req, res) => {
  const token = req.cookies.token;

  if (!token) {
      res.json(false);
      return;
  }

  try {
      // Verify token
      const verified = jwt.verify(token, process.env.JWT_SECRET);
      if (verified) {
          // If token is verified, fetch user details and include the name in the response
          const user = await User.findById(verified.id);
          if (user) {
              res.json(`User ${user.name} is logged in currently`);
          } else {
              res.json(false);
          }
      } else {
          res.json(false);
      }
  } catch (error) {
      res.json(false);
  }
});

  

//   update User
const updateUser = asyncHandler(async (req, res) => {

    const user = await User.findById(req.user._id);

    if (user) {
        const {name, email, phone, address} = user;
        user.name = req.body.name || name;
        user.phoneNom = req.body.phoneNom || phone;
        user.address = req.body.address || address;


        const updateUser = await user.save()
        res.status(200).json(updateUser);

    } else {
        res.status(404);
        throw new Error("User not found");
    }
})



// token generator

const getUserAuthToken = asyncHandler(async (req, res) => {
  const token = req.cookies.token; // Assuming you're storing the token in a cookie

  if (!token) {
      res.status(401);
      throw new Error('Unauthorized: No token provided');
  }

  res.status(200).json({ token });
});


//  set userToAdmin
const userToAdmin = asyncHandler(async (req, res) => {
    
   try {
       const {userId} = req.body;
       const user = await User.findById(userId);
       

        if (!user) {
            return res.status(404).json({ error: 'User not found' });
          }

          user.isAdmin = !user.isAdmin;
          if(user.isAdmin === true)
          {
            user.roles = "admin"
           
          } else {
            user.roles = "customer"
          }
          // Save the updated user
          await user.save();
      
          res.status(200).json({ message: 'isAdmin status updated successfully', user });

   } catch (error) {
    console.error(error);
    res
        .status(500)
        .json({error: 'Unable to convert', message: error.message}); 
    }
});


module.exports = {
    registerUser,
    loginUser,
    logoutUser,
    getUser,
    getLoginStatus,
    updateUser,
    getUserAuthToken,
    userToAdmin
};



