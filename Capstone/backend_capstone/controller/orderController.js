const asyncHandler = require("express-async-handler");
const Order = require("../models/orderModels");
const Product = require("../models/productModels");
const User = require("../models/userModel.js")
const mongoose = require('mongoose');

const createOrder = asyncHandler(async (req, res) => {
    const userId = req.params.id; // Extract userId from the URL

    try {
        // Fetch the product based on the product name from the request
        const product = await Product.findOne({name: req.body.name});

        if (!product) {
            return res
                .status(404)
                .send("Product not found.");
        }

        // Create a new order for the product
        const newOrder = new Order({
            userId: userId, // Assign 'userId' extracted from the URL
            products: [
                {
                    productId: product._id,
                    quantity: req.body.quantity, // Quantity from the request body
                }
            ],
            originalAmount: product.originalPrice, // Assuming originalPrice is the product price
            totalAmount: product.originalPrice * req.body.quantity, // Total price based on quantity
        });

        // Save the order to the database
        await newOrder.save();

        const user = await User.findById(userId);
        if (user) {
            user
                .orders
                .push(newOrder);
            await user.save();
        }

        return res.send("Order created successfully.");
    } catch (error) {
        console.error(error);
        res
            .status(500)
            .json({error: 'Unable to create order', message: error.message});
    }
});

const getUsersAndOrders = asyncHandler(async (req, res) => {
    try {

        const usersWithOrders = await User
            .find({})
            .populate('orders');
        res.json(usersWithOrders);
    } catch (error) {
        console.error(error);
        res
            .status(500)
            .json({error: 'Unable to create order', message: error.message});
    }

});

const getUsersOrder = asyncHandler(async (req, res) => {
    const userId = req.params.id;

    try {
        const userOrder = await User.findById(userId).populate('orders')
        res.json(userOrder)

    } catch (error) {
        console.error(error);
        res
            .status(500)
            .json({error: 'Unable to create order', message: error.message});
    }

});


const getAllOrders = asyncHandler(async (req, res) => {
  
    try {
        const allOrders = await Order.find();
        res.json(allOrders)

    } catch (error) {
        console.error(error);
        res
            .status(500)
            .json({error: 'Unable to create order', message: error.message});
    }

});

// addToCart
const addedProducts = asyncHandler(async (req, res) => {
    try {
      // Extract data from the request body
      const { userId } = req.body;
  
      // Check if user exists and populate their orders with product details
      const userOrder = await User.findById(userId)
        .populate({
          path: 'orders',
          populate: {
            path: 'products.productId',
            model: 'Product',
            select: 'regularPrice name description brand',
          },
        })
        .select("-password");
  
      if (!userOrder) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      // Create a new object with the desired structure, excluding unwanted fields
      const filteredUserOrder = {
        _id: userOrder._id,
        name: userOrder.name,
        email: userOrder.email,
        roles: userOrder.roles,
        isAdmin: userOrder.isAdmin,
        phoneNom: userOrder.phoneNom,
        orders: userOrder.orders.map((order) => ({
          _id: order._id,
          userId: order.userId,
          products: order.products,
        })),
        __v: userOrder.__v,
      };
  
      // Return the filtered user order
      res.json(filteredUserOrder);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Unable to update order', message: error.message });
    }
  });
  
//   productQtyChange
const productQtyChange = asyncHandler(async (req, res) => {
    try {
      // Extract data from the request body
      const { userId, productId, quantity } = req.body;
  
      // Find the user by their userId
      const user = await User.findById(userId);
  
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      // Find the specific order for the user that contains the product with the given productId
      const orderToUpdate = User.orders.find((Order) =>
      Order.products && Order.products.some((product) => Product.productId === productId)
      );
  
      if (!orderToUpdate) {
        return res.status(404).json({ error: 'Order not found' });
      }
  
      // Update the quantity of the product in the order
      if (orderToUpdate.products) {
        orderToUpdate.products.forEach((product) => {
          if (product.productId === productId) {
            product.quantity = quantity;
          }
        });
      }
  
      // Save the updated user
      await user.save();
  
      // Return the updated order or a success message
      res.json({ message: 'Order updated successfully', updatedOrder: orderToUpdate });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Unable to update order', message: error.message });
    }
  });
  
  
  
module.exports = {
    createOrder,
    getUsersAndOrders,
    getUsersOrder,
    getAllOrders,
    addedProducts,
    productQtyChange
};
