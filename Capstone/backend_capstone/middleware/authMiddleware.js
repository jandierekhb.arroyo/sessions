const asyncHandler = require("express-async-handler");
const jwt = require('jsonwebtoken'); 
const User = require('../models/userModel');


const verify = asyncHandler(async (req, res, next) => {
    try {
        const token = req.cookies.token; 
        if (!token) {
            res.json(false); // Respond with false if no token
            return;
        }

        // verify token
        const verified = jwt.verify(token, process.env.JWT_SECRET);
        // Gets User Id from token
        const user = await User.findById(verified.id).select("-password");
   
        if (!user) {
            res.status(400);
            throw new Error("User not found");
        }
        req.user = user;
        next();
    } catch (error) {
        res.status(401); // Change status to 401 for authentication failure
        next(error); // Pass the error to the Express error handler
    }
});

// Admin only
const adminOnly = asyncHandler(async (req, res, next) => {
    if (req.user && (req.user.isAdmin || req.user.roles === "admin")) {
        next();
    } else {
        res.status(400);
        throw new Error("Not authorized as an admin");
    }
});

module.exports = {
    verify,
   adminOnly,
};
