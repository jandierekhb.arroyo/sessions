import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from './AdminView';

export default function Product() {

    const {user} = useContext(UserContext);

    // State that will be used to store the products retrieved from the database
    const [products, setProducts] = useState([]);

    const fetchData = () => {
        fetch(`https://backendforcapstone3.onrender.com/product/getAllproducts`)
            .then(res => res.json())
            .then(data => {

              

                // Sets the "products" state to map the data retrieved from the fetch request
                // into several "productCard" components
                setProducts(data);

            });
    }

    // Retrieves the products from the database upon initial render of the
    // "products" component
    useEffect(() => {

        fetchData()

    }, []);


    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView productsData={products} fetchData={fetchData} />
                    :
                    <UserView productsData={products} />
            }
        </>
    )
}