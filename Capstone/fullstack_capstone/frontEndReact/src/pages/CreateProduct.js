import {useState, } from 'react';
import {Form,Button} from 'react-bootstrap';
import {  useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function CreateProduct(){

    const navigate = useNavigate();

    // const {user} = useContext(UserContext);

    //input states
    const [name,setName] = useState("");
    const [description,setDescription] = useState("");
    const [category,setCategory] = useState("");
    const [brand,setBrand] = useState("");
    const [quantity,setQuantity] = useState("");
    const [originalPrice,setOriginalPrice] = useState("");
    const [regularPrice,setRegularPrice] = useState("");
    const [SKU,setSKU] = useState("");


    function createProduct(e){

        //prevent submit event's default behavior
        e.preventDefault();

        let token = localStorage.getItem('token');
        console.log(token);

        fetch('https://backendforcapstone3.onrender.com/product/createProduct',{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                name: name,
                description: description,
                category : category,
                brand: brand,
                quantity : quantity,
                originalPrice : originalPrice,
                regularPrice: regularPrice,
                SKU : SKU
               

            })
        })
        .then(res => res.json())
        .then(data => {

            //data is the response of the api/server after it's been process as JS object through our res.json() method.
            console.log(data);

            if(data){
                Swal.fire({

                    icon:"success",
                    title: "Product Added"

                })

                navigate("/adminTable");
            } else {
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful Product Creation",
                    text: data.message

                })
            }

        })

        
    }

    return (

            <>
                <h1 className="my-5 text-center">Add Product</h1>
                <Form onSubmit={e => createProduct(e)}>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => {setName(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Category:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Category" required value={category} onChange={e => {setCategory(e.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Brand:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Brand" required value={brand} onChange={e => {setBrand(e.target.value)}}/>
                    </Form.Group>

                    
                    <Form.Group>
                        <Form.Label>Quantity:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Quantity" required value={quantity} onChange={e => {setQuantity(e.target.value)}}/>
                    </Form.Group>
        
                    <Form.Group>
                        <Form.Label>Original Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Original Orice" required value={originalPrice} onChange={e => {setOriginalPrice(e.target.value)}}/>
                    </Form.Group>


                    <Form.Group>
                        <Form.Label>Regular Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Regular Price" required value={regularPrice} onChange={e => {setRegularPrice(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>SKU:</Form.Label>
                        <Form.Control type="text" placeholder="Enter SKU" required value={SKU} onChange={e => {setSKU(e.target.value)}}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" className="my-5">Submit</Button>
                </Form>
            </>
           
    )


}