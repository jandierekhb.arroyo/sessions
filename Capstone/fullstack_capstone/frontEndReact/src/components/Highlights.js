import { Row, Col, Card } from 'react-bootstrap';

export default function Hightlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Understanding </h2>
			        </Card.Title>
			        <Card.Text>
					essential in managing and maintaining various digital platforms, systems, and organizations. These privileges grant individuals elevated access and control over critical aspects of a system, such as user management, configuration settings, and data security. Administrators play a pivotal role in ensuring the smooth operation of a system and are responsible for making vital decisions that can impact the overall functionality and security of an environment.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Admin Privileges</h2>
			        </Card.Title>
			        <Card.Text>
					Provide unrestricted access to all system resources and commands, allowing administrators to make comprehensive changes and modifications. Database administrators, on the other hand, manage and maintain the integrity and security of databases. Additionally, network administrators are responsible for configuring and maintaining network infrastructure, ensuring that data flows smoothly and securely. Understanding these different types of admin privileges is crucial for effectively managing and securing digital systems.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Importance </h2>
			        </Card.Title>
			        <Card.Text>
					paramount importance in the digital age. Admin accounts are high-value targets for malicious actors seeking to gain unauthorized access or control over a system. Strong password policies, multi-factor authentication, and restricted access based on the principle of least privilege are critical strategies in ensuring the security of admin privileges. Furthermore, regular audits and monitoring of admin actions help detect and prevent unauthorized or malicious activities. Admin privileges are a double-edged sword, and a comprehensive security approach is necessary to protect both the administrator and the system from potential threats.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}