import { Card, Button } from 'react-bootstrap';

import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { _id, name, description, regularPrice } = productProp;


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {regularPrice}</Card.Text>
	        <Link className="btn btn-primary" to={`/product/getProduct/${_id}`}>Details</Link>
	    </Card.Body>
	</Card>	
	)
}

// Check if the ProductCard component is getting the correct prop types
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		regularPrice: PropTypes.number.isRequired
	})
}