const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Full name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        unique: true,
        trim: true,
        match: [emailRegex, "Please Enter a valid Email"]
    },
    password: {
        type: String,
        required: [true, "Password is required"],
        minLength: [6, "Password must be at least 6 characters"]
    },
    roles: {
        type: String,
        required: [true],
        default: "customer",
        enum: ["customer", "admin"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    phoneNom: {
        type: String,
        required: [true, "CP number is required"]
    },  
    orders: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Order', 
        }
    ],
});

// Apply hashing before checking if the code has less than 6 characters
userSchema.pre("save", async function(next) {
    if (!this.isModified("password")) {
        return next();
    }

    // Hash Password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(this.password, salt);
    this.password = hashPassword;
    next();
});

module.exports = mongoose.model("User", userSchema);
