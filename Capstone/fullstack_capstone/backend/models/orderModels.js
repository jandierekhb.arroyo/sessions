const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'UserId is required!']
    },
    products: [
        {
            name :  {
                type: String,
                required: [true, 'name is required!'],
                trim: true
            },
            productId: {
                type: String,
                required: [true, 'productId is required!'],
                trim: true
            },
            quantity: {
                type: Number,
                required: [true, 'Please add a quantity'],
                trim: true
            }
        }
    ],
    originalAmount: {
        type: Number,
        required: [true, 'originalAmount is required!'],
        trim: true
    },

    totalAmount: {
        type: Number,
        required: [true, 'totalAmount is required!'],
        trim: true
    },
    purchasedOn: {
        type: Date,
        default: Date.now,
        trim: true
    }
});

module.exports = mongoose.model('Order', orderSchema); 
