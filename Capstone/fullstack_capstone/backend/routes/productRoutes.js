const express = require('express');
const router = express.Router();
const { createProduct, getAllproducts, getProduct, archiveProduct, activateProduct, 
    allArchiveproducts, allActiveProducts, updateProduct, searchProductByName} = require('../controller/productController'); 
const {verify, adminOnly} = require("../middleware/authMiddleware")
const asyncHandler = require('express-async-handler');


// Create Product
router.post('/createProduct', asyncHandler(createProduct));

// Get Products getAllproducts
router.get('/getAllproducts', asyncHandler(getAllproducts));

// Get Product
router.get('/getProduct/:id', asyncHandler(getProduct));

//  Archive Product
router.put('/archiveProduct/:productId',  asyncHandler(archiveProduct));

// Activate a Product
router.put('/activateProduct/:productId',  asyncHandler(activateProduct));

//  Update Product
router.put('/updateProduct/:id', asyncHandler(updateProduct));

//  get all active products
router.get('/allActiveProducts/:id', verify, adminOnly, asyncHandler(allActiveProducts));

// get all allArchiveproducts
router.get('/allArchiveproducts/:id', verify, adminOnly, asyncHandler(allArchiveproducts));

// search product by name
router.post('/searchByName', asyncHandler(searchProductByName)); 
module.exports = router;
 