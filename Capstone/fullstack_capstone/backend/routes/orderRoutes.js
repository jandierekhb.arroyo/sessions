const express = require('express');
const router = express.Router();
const {createOrder, getUsersAndOrders, getUsersOrder, productQtyChange, getAllOrders, addedProducts} = require(
    '../controller/orderController'
);
const asyncHandler = require('express-async-handler');
const {verify, adminOnly} = require("../middleware/authMiddleware")

// Create a new order
router.post('/createOrder/:id', asyncHandler(createOrder));

// User and their orders
router.get('/getUsersAndOrders', asyncHandler(getUsersAndOrders));

//  user and their order getUsersOrder
router.get('/getUsersOrder/:id', asyncHandler(getUsersOrder));

router.get('/getAllOrders', asyncHandler(getAllOrders))

router.post('/addedProducts', asyncHandler(addedProducts))

// Change Product quantity
router.post('/productQtyChange', asyncHandler(productQtyChange))

module.exports = router;
