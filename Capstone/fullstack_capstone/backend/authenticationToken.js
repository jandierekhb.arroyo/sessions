// tokenUtils.js

const jwt = require('jsonwebtoken');

const generateAuthToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, { expiresIn: "1d" });
};

module.exports = {
    generateAuthToken,
};


