function countLetter(letter, sentence) {
    let result = 0;
    // Check first whether the letter is a single character. If letter is a single
    // character, count how many times a letter has occurred in a given sentence
    // then return count. If letter is invalid, return undefined.
    if (letter.length === 1) {
        let count = 0;
        sentence = sentence.toLowerCase();
        letter = letter.toLowerCase();

        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                count++;
            };
        };
        return count;

    } else {
        return undefined;
    }
}
const letter = countLetter('i', "misssipi");
console.log(letter);


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters. The function
    // should disregard text casing before doing anything else. If the function
    // finds a repeating letter, return false. Otherwise, return true.
    text = text.toLowerCase();
    const encounteredLetters = [];

    for (let i = 0; i < text.length; i++) {
        const letter = text[i];

        if (encounteredLetters[letter]) {
            return false;
        }
        encounteredLetters[letter] = true;
    }
    return true;
}

console.log(isIsogram('Dermatoglyphics'));
console.log(isIsogram('decamon'));
console.log(isIsogram('mama'));


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    switch (true) {
        case age < 13:
            return undefined;
        case (age >= 13 && age <= 21) || age >= 65:
            const discountAmount = (price * 20) / 100;
            const discountedPrice = price - discountAmount;
            const roundedDiscountedPrice = discountedPrice.toFixed(2);
            return `${roundedDiscountedPrice}`;
        case (age >= 22 && age <= 64):
            const roundedPrice = price.toFixed(2);
            return `${roundedPrice}`;
        default:
            const roundedOriginalPrice = price.toFixed(2);
            return `$${roundedOriginalPrice}`;
    }
}

console.log(purchase(13, 2000));
console.log(purchase(15, 2000));
console.log(purchase(22, 2000));


function findHotCategories(items) {
    // Find categories that has no more stocks. The hot categories must be unique;
    // no repeating categories. The expected output after processing the items array
    // is ['toiletries', 'gadgets']. Only putting return ['toiletries', 'gadgets']
    // will not be counted as a passing test during manual checking of codes.

    const hotCategories = [];
    for (const item of items) {
        if (item.stocks === 0) {
            if (!hotCategories.includes(item.category)) {
                hotCategories.push(item.category)
            }
        }
    }

    return hotCategories;
}

const items = [
    {
        id: 'tltry001',
        name: 'soap',
        stocks: 14,
        category: 'toiletries'
    }, {
        id: 'tltry002',
        name: 'shampoo',
        stocks: 8,
        category: 'toiletries'
    }, {
        id: 'tltry003',
        name: 'tissues',
        stocks: 0,
        category: 'toiletries'
    }, {
        id: 'gdgt001',
        name: 'phone',
        stocks: 0,
        category: 'gadgets'
    }, {
        id: 'gdgt002',
        name: 'monitor',
        stocks: 0,
        category: 'gadgets'
    }
]

console.log(findHotCategories(items));

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes. 
    flyingVoters = [];
    for( const voter of candidateA){
        if(candidateB.includes(voter)){
            flyingVoters.push(voter);
        }
    }
    return flyingVoters;
}
   const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
   const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

   console.log(findFlyingVoters(candidateA, candidateB));

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
}