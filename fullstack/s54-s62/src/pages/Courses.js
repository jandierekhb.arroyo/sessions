import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';
import {useEffect, useState, useContext} from 'react';
import AdminView from './AdminView'
import UserContext from '../UserContext';

export default function Courses() {
	// Checks to see if the mock database was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);

	const {user} = useContext(UserContext);

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		// get all active courses
		//http://localhost:4000/courses/
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			
			setCourses(user.isAdmin ? data : data.map((course) => <CourseCard key={course._id} courseProp={course} />));

		})
	})
	
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
        .then(res => res.json())
        .then(data => {

            // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
            setCourses(data);

        });
    }

    useEffect(() => {

        fetchData()

    }, []);

	return(
		<>
		{
			(user.isAdmin) ?
				<AdminView coursesData={courses}/>
			:
			<>
			{courses}
			</>
		}
		</>
	)
}