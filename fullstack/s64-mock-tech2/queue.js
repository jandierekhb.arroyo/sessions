let collection = [];

function print(){
    return collection;
}
function enqueue(item){
    return (collection.push(item), collection);
}

function dequeue(item){
    return (collection.shift(item), collection);
}

function front(){
     return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    return collection.length === 0;
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};